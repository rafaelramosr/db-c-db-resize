const sharp = require('sharp');
const download = require('image-downloader');
const { BASE_URL, CHARACTERS } = require('./config');


const promiseImages = CHARACTERS.map((character) => {
  const options = {
    url: `${BASE_URL}${character}`,
    dest: `${__dirname}/original-images`,
  };
  return download.image(options);
});

Promise.all(promiseImages)
  .then((filenames) => {
    filenames.forEach(({ filename }) => {
      const [directory, name] = filename.split('/original-images/');
      sharp(filename).resize(120).png().toFile(`${directory}/images/sm/${name}`);
      sharp(filename).resize(320).png().toFile(`${directory}/images/md/${name}`);
      sharp(filename).resize(520).png().toFile(`${directory}/images/lg/${name}`);
    });
  })
  .catch((err) => console.error(err));
